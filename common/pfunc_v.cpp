#include "pfunc_v.h"

#ifdef WIN32
#include <Winsock2.h>
#include <stdio.h>  
#include <stdlib.h>
#include <combaseapi.h>
#define GUID_LEN 64  
#else
#include <string.h>
#include <uuid/uuid.h>  
#endif
#include "pfunc_print.h"

std::string pyfree::getUUID()
{
#ifdef WIN32
	char buffer[GUID_LEN] = { 0 };
	GUID guid;

	if (CoCreateGuid(&guid))
	{
		Print_WARN("create guid error\n");
		return "";
	}
	_snprintf(buffer, sizeof(buffer),
		"%08X-%04X-%04x-%02X%02X-%02X%02X%02X%02X%02X%02X",
		guid.Data1, guid.Data2, guid.Data3,
		guid.Data4[0], guid.Data4[1], guid.Data4[2],
		guid.Data4[3], guid.Data4[4], guid.Data4[5],
		guid.Data4[6], guid.Data4[7]);
	Print_NOTICE("guid: %s\n", buffer);
	return std::string(buffer);
#else
	uuid_t uuid;
	uuid_generate(uuid);

	char buf[64] = { 0 };
	uuid_unparse(uuid, buf);
	/*
	snprintf(buf,
	sizeof(buf),
	"%08x-%04x-%04x-%02x%02x-%02x%02x%02x%02x%02x%02x",
	uuid.data1, uuid.data2, uuid.data3,
	uuid.data4[0], uuid.data4[1], uuid.data4[2], uuid.data4[3],
	uuid.data4[4], uuid.data4[5], uuid.data4[6], uuid.data4[7]);
	*/
	return std::string(buf);
#endif
};