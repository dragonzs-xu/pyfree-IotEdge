#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef _PFUNC_CODE_H_
#define _PFUNC_CODE_H_
/***********************************************************************
  *Copyright 2020-03-06, pyfree
  *
  *File Name       : pfunc_code.h
  *File Mark       : 
  *Summary         : 帧数据处理相关的函数集
  *
  *Current Version : 1.00
  *Author          : pyfree
  *FinishDate      :
  *
  *Replace Version :
  *Author          :
  *FinishDate      :

 ************************************************************************/

namespace pyfree
{
	/**
	 * frame code,帧数据编码
	 * @param buff {const unsigned char* } 源字符串
	 * @param len {const int} 源字符串长度
	 * @param outbuf {unsigned char *} 编码结果
	 * @return {int} 编码结果长度
	 */
	int code(const unsigned char *buff, const int len, unsigned char *outbuf);
	/**
	 * frame uncode,帧数据解码,和code配套使用
	 * @param buff {const unsigned char* } 源字符串
	 * @param len {const int} 源字符串长度
	 * @param outbuf {unsigned char *} 解码结果
	 * @return {int} 解码结果长度
	 */
	int uncode(const unsigned char *buff, int len, unsigned char *outbuf);
	/**
	 * crc校验
	 * @param ptr {unsigned char*} } 待校验字符串
	 * @param len {unsigned int} } 待校验字符串长度
	 * @return {unsigned int } 校验结果,即crc值
	 */
	unsigned int  crc16(unsigned char *ptr, unsigned int len);
};

#endif
