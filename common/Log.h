#ifndef CHANNELLOG_H
#define CHANNELLOG_H
/***********************************************************************
  *Copyright 2020-03-06, pyfree
  *
  *File Name       : Log.h
  *File Mark       : 
  *Summary         : 
  *日志类,单体类,基于c++库acl_master的日志类库实现
  *
  *Current Version : 1.00
  *Author          : pyfree
  *FinishDate      :
  *
  *Replace Version :
  *Author          :
  *FinishDate      :

 ************************************************************************/
#include "lib_acl.h"
#include "acl_cpp/lib_acl.hpp"

#include "queuedata.h"

enum eLogType 
{
	MsgInfo		= 1,	//一般通知
	MsgWarn		= 2,	//消息告警
	MsgError	= 3,	//业务异常
	MsgFatal 	= 4,	//逻辑异常
	MsgPanic	= 5		//软件bug
};

class log_item
{
public:
	log_item():type(0)
	{
		memset(szBuffer, 0, 1024);
	};

	~log_item()
	{
	};
	log_item& operator=(const log_item &rval)
	{
		if (this != &rval) {
			memset(szBuffer, 0, 1024);
			memcpy(szBuffer, rval.szBuffer, strlen(rval.szBuffer));
			type = rval.type;
		}
		return *this;
	};
	
	int 	type;			//日志类型
	char   	szBuffer[1024]; //日志描述
};

class CLogger : public acl::thread
{
public:
	CLogger();
	~CLogger();
	void* run();
public:
	/**
	 * 设置日志
	 * @param eLogType {eLogType} 日志类型
	 * @param lpszFormat {char*} 日志格式化描述的第一个参数,
	 * @return {void} 
	 */
	void Log(const eLogType type, const char* lpszFormat, ...);
	/**
	 * 设置记录日志的同时是否允许屏幕打印输出
	 * @param eLogType {eLogType} 日志类型
	 * @return {void} 
	 */
	void set_printf_flag(bool flag_);
	static CLogger* createInstance( void );
private:
	/**
	 * 记录日志到文件或信道
	 * @param it {log_item} 日志描述信息
	 * @return {void} 
	 */
	void WriteLog(log_item it);
private:
	static CLogger* m_pLogInstance;
	bool running;
	bool printf_flag;				//是否允许屏幕打印输出显示
	QueueData<log_item> *log_queue;	//日志缓存队列
	std::string log_file;			//日志文件名,格式为yyyy-MM-dd__gather.log
};
#endif
