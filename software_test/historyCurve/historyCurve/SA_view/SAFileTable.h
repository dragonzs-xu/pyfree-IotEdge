#ifndef SA_FILE_TABLE_H
#define SA_FILE_TABLE_H

#include "tableview.h"

QT_BEGIN_NAMESPACE
class QAbstractItemModel;
class QContextMenuEvent;
class QMouseEvent;
class QWidget;
QT_END_NAMESPACE

class SAFileTableView : public MyTableView
{
	Q_OBJECT
public:
    SAFileTableView(QAbstractItemModel * model,QWidget * parent = 0);
    ~SAFileTableView();

    void clear();
    void addFileToList(QString _name);
protected:
	virtual void timerEvent(QTimerEvent * event);
    void contextMenuEvent ( QContextMenuEvent * event );
    void mouseDoubleClickEvent ( QMouseEvent * event );
private:
    void win_init();
    void loadFileList();
signals:
	void fileSelect(QString _name);
public slots:
    void saveFile();
private slots:

private:
	QString m_dbDir;
	int		m_iTimer;
};

#endif // SA_FILE_TABLE_H
