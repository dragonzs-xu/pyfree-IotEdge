#ifndef SADB_H
#define SADB_H

#include <QSet>

#include "sqliteModel/CSQLite.h"
#include "SA_db/SADef.h"

class SADB : public CSQLiteDB
{
public:
	SADB(void);
	virtual ~SADB(void);
	void Close();
	bool Open(char * szFilePath = NULL);
	bool IsOpen();
	long long GetLastFT(void);

	bool AddYARecorder(QList<SADataItem> &_datas);
	bool getSADataQuery(QList<SADataItem> &_datas,QSet<int> &_devids
		,QSet<int> &_idxs,SAQueryConf _qconf=SAQueryConf());
protected:

private:

private:
	bool m_bOpen;
};
#endif //SADB_H
