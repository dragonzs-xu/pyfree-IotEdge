#ifndef SADEF_H
#define SADEF_H

#include <QList>
#include <QString>
#include <QDateTime>

#include "plot/pfunc_qt.h"

struct SADataItem
{
    void ToQT()
    {
        dt_QDT = PFUNC_QT::ChangeLong(Time);
        dt_STR = PFUNC_QT::ChangeDateTime(dt_QDT);
        val_STR = QString("%1").arg(double(Value), 0, 'f', 3);
    };
    int         Dev_Index;
    int         YC_Index;
    long long   Time;
    float       Value;
    QDateTime   dt_QDT;
    QString     dt_STR;
    QString     val_STR;
};

struct SAQueryConf
{
    SAQueryConf() : devid(-1),idx(-1),tvStart(0),tvStop(0),m_limit(-1)
    {

    };
    int devid;
    int idx;
    long long tvStart;
    long long tvStop;
    int m_limit;
};

struct LimitData
{
    float       minValue;
    QString     min_dt_STR;
    float       maxValue;
    QString     max_dt_STR;
};
#endif //SADEF_H
