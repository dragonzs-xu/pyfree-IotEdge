#include "plotData.h"

MyInterval::MyInterval(double vl, double minv, double maxv) 
	: value(vl), min(minv), max(maxv)
{

};
 
MyInterval::~MyInterval(){

};

double MyInterval::getvalue(){
	return value;
};
   
double MyInterval::getmin(){
	return min;
};
    
double MyInterval::getmax(){
	return max;
};
/////////////////////////////////////

MyColor::MyColor(int rv, int gv, int bv, int av)
	: r(rv), g(gv), b(bv), a(av)
{
    init();
};
    
MyColor::~MyColor(){

};

int MyColor::red(){
	return r;
};
    
int MyColor::green(){
	return g;
};
    
int MyColor::blue(){
	return b;
};
    
int MyColor::apha(){
	return a;
};

void MyColor::init(){
    r = changeColorValue(r);
    g = changeColorValue(g);
    b = changeColorValue(b);
    a = changeColorValue(a);
};

int MyColor::changeColorValue(int vl){
    int value = 255;
    if(vl<0)
    {
        value = 0;
    }else if(vl>255)
    {
        value = 255;
    }else{
        value = vl;
    }

    return value;
};

///////////////////////////////////////////////

MySetSample::MySetSample(std::vector<double> setVal, double val, double xWitchVal)
        : set(setVal) , value(val), xWitch(xWitchVal)
{
};
    
MySetSample::~MySetSample(){

};

std::vector<double> MySetSample::getSet(){
    return set;
};

double MySetSample::getValue(){
    return value;
};

double MySetSample::getWitch(){
    return xWitch;
};

////////////////////////////////

CurveInterval::CurveInterval()
    : from(0), to(0)
{
};
    
CurveInterval::CurveInterval(int fVal, int tVal)
    : from(fVal), to(tVal)
{
};

CurveInterval::~CurveInterval(){

};

int CurveInterval::getFrom() const{ 
    return from;
};
    
int CurveInterval::getTo() const{ 
    return to;
};
