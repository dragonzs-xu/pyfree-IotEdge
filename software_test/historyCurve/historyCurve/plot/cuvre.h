#ifndef CUVRE_H
#define CUVRE_H
/////////////////////////////////////////////////////////////////////////////////////
#include <QVector>
#include <QString>
#include <QBrush>
#include <QColor>
#include <QPen>

#include "qwt/qwt_plot_curve.h"
#include "qwt/qwt_symbol.h"
#include "plotData.h"

class MyQwtPlotCurve;

class Cuvre
{
public:
    Cuvre(QString title,QwtPlotCurve::CurveStyle style);
    ~Cuvre();

    QString getTitle() ;
    void attach( QwtPlot *plot );
    void detach();
    void addPoint(QPointF point);
    void setBaseline(double baseline);
    void setBrush(QBrush brush);

    void setSamples();
    void setStyle(QwtPlotCurve::CurveStyle style);
    void setPen(QColor color);
    void setPen(QPen pen);
    void setSymbolStyle(QwtSymbol::Style style);
    void setSymbolBrush(QBrush brush);
    void setSymbolPen(QPen pen);
    void setSymbolSize(QSize size);

    void addInterval(const int from, const int to);
    void setCurveMove( const double x_Move, const double y_Move);
private:
    QVector< QPointF > val;
    QVector<CurveInterval> m_CurveInterval;
    MyQwtPlotCurve *curve;
};

#endif //CUVRE_H
