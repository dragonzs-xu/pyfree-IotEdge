#ifndef PLOTHISTOGRAM_H
#define PLOTHISTOGRAM_H

#include "qwt/qwt_plot_histogram.h"
#include "qwt/qwt_scale_map.h"
#include "qwt/qwt_symbol.h"
#include "plotData.h"

////////////////////////////////////////////////////////////////////////////////////
class MyQwtPlotHistogram : public QwtPlotHistogram
{
public:
    explicit MyQwtPlotHistogram(const QString &title = QString::null);
    explicit MyQwtPlotHistogram(const QwtText &title);
    ~MyQwtPlotHistogram();

    void setMyColorData(QVector< QColor >* color);
    virtual void drawSeries( QPainter *p,
        const QwtScaleMap &xMap, const QwtScaleMap &yMap,
        const QRectF &canvasRect, int from, int to ) const;
protected:
    void drawMyColumns( QPainter *,
        const QwtScaleMap &xMap, const QwtScaleMap &yMap,
        int from, int to ) const;
    virtual QwtColumnRect columnRect( const QwtIntervalSample &,
        const QwtScaleMap &, const QwtScaleMap & ) const;

    virtual void drawColumn( QPainter *, const QwtColumnRect &,
        const QwtIntervalSample & ) const;
protected:
    void dataChanged();
	size_t dataSize() const;
    QRectF dataRect() const;

    void setRectOfInterest( const QRectF &rect );
private:
    void init();
private:
    QVector< QColor >* m_Color;
};

#endif //PLOTHISTOGRAM_H
