#include "mymenubar.h"

MyMenuBar::MyMenuBar(QWidget *parent) :
    QMenuBar(parent)
{
    init();
}

MyMenuBar::~MyMenuBar()
{

}

void MyMenuBar::init()
{
    setAutoFillBackground(true);
    setPalette(QColor(150,150,150));
//    setObjectName("MainMenuBar");

    setLayoutDirection(Qt::LeftToRight);

    QFont _font("Courier New", 10);
    setFont(_font);
}
