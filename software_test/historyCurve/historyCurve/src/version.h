#pragma once

#define VERSION_BUILD_NUMBER 		1001
#define STR_BUILD_NUMBER 			"1001"
#define STR_VERSION_BUILD 			"(B1006 2020/05/08 10:52:07)"
#define STR_VERSION_DATE 			"2020/05/08 10:52:07"
#define STR_VERSION_URL 			""
#define STR_VERSION_NOW 			"2020/05/10 13:52:07"
#define NSTR_VERSION_NOW 		 2020/05/10 13:52:07

#define STR_VERSION_VERSION			"V1.01"
#define STR_VERSION_COMPANY			"珠海***科技有限公司"
#define STR_VERSION_FILEDESC			"边缘服务历史数据展示工具"
#define STR_VERSION_FILEVER			STR_VERSION_VERSION
#define STR_VERSION_LEGAL				"pyfree All rights reserved"
#define STR_VERSION_PRODUCTNAME 			"history_curve_demo"
#define STR_VERSION_TELE				"+86 756 0000000"
#define STR_VERSION_FAX				"+86 756 0000000"
#define STR_VERSION_WEB				"https://gitee.com/pyzxjfree/pyfree-IotEdge"
#define STR_VERSION_POSTCODE			"519080"
#define STR_VERSION_ADDRESS			"珠海"
