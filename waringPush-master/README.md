# pyfree-waringPush-master

#### 介绍
用于实现告警信息对外通告或发布，告警信息来自监控调度，与监控软件（pyfree-monitor-master）配套使用

#### 软件说明  

##### 架构概述 
* 1)总概述:  
    告警推送软件通过不同的发布接口实现告警信息对外推送  
    ![组件及模块](/res/for_doc/waringPush_com.PNG)  

* 2)类图:   
    ![告警推送软件主要类](/res/for_doc/waringPush_class.PNG)   
    
##### 功能概述  

* 1) 告警汇聚：  
    接收来自监控调度软件的告警信息，由于是单向通信，暂时采用udp通信方式,考虑到告警内容的扩展性,采用json格式,后续会加入hmac_sha校验。        
  
* 2)对外接口：  
    * 其一，实现了告警信息推送日志接口  
    * 其二，实现了告警信息推送阿里云短信服务的接口  
    * 其三，实现了邮件发布的接口(测试了163邮箱提供的邮件服务)  

#### 编译
##### 一、依赖
1.  acl_master  
acl_master是一个跨平台c/c++库，提供了网络通信库及服务器编程框架，同时提供更多的实用功能库及示例，具体编译与实现请参考其说明文档。  
项目地址: https://gitee.com/acl-dev/acl 或 https://github.com/acl-dev/acl  
技术博客：https://www.iteye.com/blog/user/zsxxsz  

做了两处源码调整:  
由于acl提供的日志记录的时间只到秒级别，本项目需要毫秒的级别，因此修改了一下其源码，  
在lib_acl/src/stdlib/acl_mylog.c文件中，将acl_logtime_fmt函数实现调整为：  
```
void acl_logtime_fmt(char *buf, size_t size)  
{  
	//time_t	now;  
	struct timeval tm0;  
	gettimeofday(&tm0, NULL);  
	time_t	now = tm0.tv_sec;  
#ifdef	ACL_UNIX  
	struct tm local_time;  
  
	//(void) time (&now);  
	(void) localtime_r(&now, &local_time);  
	strftime(buf, size, "%Y/%m/%d %H:%M:%S", &local_time);  
	sprintf(buf, "%s.%03d ", buf,(int)(tm0.tv_usec/1000));  
#elif	defined(ACL_WINDOWS)  
	struct tm *local_time;  
  
	//(void) time (&now);  
	local_time = localtime(&now);  
	strftime(buf, size, "%Y/%m/%d %H:%M:%S", local_time);  
	sprintf(buf, "%s.%03d ",buf, (int)(tm0.tv_usec/1000));  
#else  
# error "unknown OS type"  
#endif  
}   
```
如果不需要毫秒级的时间格式，就不必修改。  

其二:  
需要修改acl库的源码,在lib_acl/src/stdlib/iostuff/acl_readable.c的int acl_readable(ACL_SOCKET fd)函数中
```
fds.events = POLLIN | POLLPRI;
```
由于win中调用了WSAPoll函数，而Winsock provider不支持POLLPRI与POLLWRBAND,调整为:
```
fds.events = POLLIN | POLLPRI;
#ifdef ACL_WINDOWS
fds.events &=~(POLLPRI|POLLWRBAND);
#endif
```

2. iot_aliyun  
iot_aliyun目录是阿里云物联网平台提供的c++_SDK包编译的头文件和库，采用的是iotkit-embedded-2.3.0.zip源码编译的。  
最新版本可以去阿里云物联网平台官网下载，新版本有些函数会有变更，本调度软件涉及阿里云物联网接口部分需要作出调整。  
告警推送软件用来实现与阿里云短信服务实现短信告警  
 

##### 二、项目编译：

> linux编译
项目编译需要cmake+gcc支持，目前本项目的编译环境是centos7.3，由于需要将git相关信息写入程序,需要安装git  
或在CMakeLists.txt中注销:"execute_process(COMMAND /bin/bash ../build.sh)"  
编译命令类似如下:  
```
cd waringPush-master 
mkdir build-linux 
cd build-linux 
cmake .. 
make
```

> win编译
当前win编译测试采用vs2015+cmake编译,例子如下  
备注:若需要32为支持请去掉Win64指定,msbuild为vs的命令编译工具,可以直接vs打开工程文件编译    
打开vs的命令行工具  
```
cd waringPush-master  
mkdir build-win  
cd build-win  

cmake -G "Visual Studio 14 2015 Win64" -DCMAKE_BUILD_TYPE=Release ..  
msbuild pyfree-waringPush.sln /p:Configuration="Release" /p:Platform="x64"
或
cmake -G "Visual Studio 14 2015 Win64" -DCMAKE_BUILD_TYPE=Debug ..  
msbuild pyfree-waringPush.sln /p:Configuration="Debug" /p:Platform="x64"
```
由于acl第三方库win静态库有点大,本项目debug版本库不上传编译好的库,请自行编译,需要做一处调整
需要修改acl库的源码,在lib_acl/src/stdlib/iostuff/acl_readable.c的int acl_readable(ACL_SOCKET fd)函数中
```
fds.events = POLLIN | POLLPRI;
```
由于win中调用了WSAPoll函数，而Winsock provider不支持POLLPRI与POLLWRBAND,调整为:
```
fds.events = POLLIN | POLLPRI;
#ifdef ACL_WINDOWS
fds.events &=~(POLLPRI|POLLWRBAND);
#endif
```
#### demo示例

> linux  
1. 环境搭建  
主机win64系统  

虚拟机  
采用VMware工具创建虚拟机,centos7.3系统,本样例的网络地址设置192.168.174.130,  
开启虚拟机的共享文件夹功能，将项目挂载到虚拟机上  
  
2.  程序配置  
编译的告警推送软件输出在demo-project/waringPush  
程序启动有根据磁盘或网卡校验的License约束,与采集软件一致  
因此需要向生成License的sn.txt输出文件，启动虚拟机，进入其目录执行指令构建：  
./SWL 0 22 或 ./SWL 1 22  
生成License的sn.txt，如果与采集软件部署在同一台设备，生成一次就能拷贝过来使用  
如果没用SWL程序，去顶级目录下的swLicense项目编译生成,支持cmake编译  
  
3.  项目配置  
appconf.xml：主要设置程序需要的程序运行参数、告警推送接口配置信息等  

> win64  
1.  程序配置  
编译的告警推送软件输出在demo-project/waringPush  
程序启动有根据磁盘或网卡校验的License约束,与采集软件一致  
因此需要向生成License的sn.txt输出文件,进入其目录执行指令构建：  
SWL.exe 0 22 或 SWL,exe 1 22  
生成License的sn.txt，如果与采集软件部署在同一台设备，生成一次就能拷贝过来使用  
如果没用SWL程序，去顶级目录下的swLicense项目编译生成,支持cmake编译  
  
服务安装,管理员启动cmd,进入demo-project/waringPush目录：  
安装:pyfree-waringPush.exe install  
卸载:pyfree-waringPush.exe uninstall  
在任务管理器或服务管理页面可以启停服务或配置其服务相关信息  
更新:在任务管理器停止服务,拷贝pyfree-waringPush.exe覆盖完成更新  

2.  项目配置  
appconf.xml：主要设置程序需要的连接信息、对外告警推送接口信息等       

> 阿里云短信服务支持  

