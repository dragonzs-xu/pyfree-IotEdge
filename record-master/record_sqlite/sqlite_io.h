#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#ifndef _SQLITE_H_
#define _SQLITE_H_
/***********************************************************************
  *Copyright 2020-03-06, pyfree
  *
  *File Name       : sqlite_record.h
  *File Mark       :
  *Summary         : 
  *数据存储接口实现,通过sqlite-API实现
  *
  *Current Version : 1.00
  *Author          : PengYong
  *FinishDate      :
  *
  *Replace Version :
  *Author          :
  *FinishDate      :

 ************************************************************************/
#include <set>
#include <list>

#include "CSQLite.h"
#include "dbtypedef.h"

class SqliteIO : public CSQLiteDB
{
public:
	SqliteIO(void);
	virtual ~SqliteIO(void);
	/////////////////////////////////////////////////////////////////////
	/**
	 * 关闭sqlite文件
	 * @return {void}
	 */
	void Close();
	/**
	 * 打开sqlite文件
	 * @param szFilePath {char* } 文件名
	 * @return {bool}
	 */
	bool Open(char * szFilePath = NULL);
	/**
	 * 判定当前是否处于打开状态
	 * @return {bool}
	 */
	bool IsOpen();
	/**
	 * 获取当前sqlite文件中存储列表内最新更新项时间
	 * @return {long long} 毫秒
	 */
	long long GetLastFT(void);
	/**
	 * 将记录信息写入数据
	 * @param _datas {list } 记录信息集
	 * @return {bool}
	 */
	bool AddYARecorder(std::list<SADataItem> &_datas);
	/**
	 * 从当前打开的数据库中获取记录信息
	 * @param _datas {list } 输出记录信息集
	 * @param _devids {set } 输出设备编号集
	 * @param _idxs {set } 输出点编号集
	 * @param _qconf {SAQueryConf } 查询条件
	 * @return {bool}
	 */
	bool getSADataQuery(std::list<SADataItem> &_datas, std::set<int> &_devids
		,std::set<int> &_idxs,SAQueryConf _qconf=SAQueryConf());
private:
	bool m_bOpen;
};
#endif //SADB_H
