#ifndef _GSMMSG_H_
#define _GSMMSG_H_
/***********************************************************************
  *Copyright 2020-03-06, pyfree
  *
  *File Name       : gsmmsg.h
  *File Mark       : 
  *Summary         : 串口接口,实现串口数据通信
  *
  *Current Version : 1.00
  *Author          : pyfree
  *FinishDate      :
  *
  *Replace Version :
  *Author          :
  *FinishDate      :

 ************************************************************************/
#include <stdio.h>
#include <string>
#include <iostream>
#include <memory.h>
#include "ctb-0.16/serport.h"

class gsmmsg : public ctb::SerialPort
{
public:
	gsmmsg();

	/**
	 * 下发控制或查询指令,并等待响应返回
     * @param text {char*} 下发报文信息,发送大小256
	 * @param atres {char*} 返回报文信息，缓存大小256
	 * @param exception {char*} 异常描述,缓存大小256
	 * @param wf {bool} 下发是否成功
	 * @param acsiif {bool} acsii编码支持，true:返回报文信息为acsii格式,false:返回报文信息为hex格式
	 * @return {bool } 执行成功还是出现异常
	 */
	bool sendmsg(const char * text, char *atres, char* exception,bool &wf,bool acsiif=false);
	/**
	 * 下发控制或查询指令长度检测
     * @param text {char*} 下发报文信息
	 * @param exception {char*} 异常描述
	 * @return {bool } 下发指令长度是否合适
	 */
	bool lencheck(const char * text,char* exception);
	/**
	 * 下发控制或查询指令写入
     * @param text {char*} 下发报文信息
	 * @param textlen {int} 下发报文信息大小
	 * @param exception {char*} 异常描述
	 * @param eplen {int} 异常描述缓存大小
	 * @param acsiif {bool} acsii编码支持，true:返回报文信息为acsii格式,false:返回报文信息为hex格式
	 * @return {bool } 写入是否成功
	 */
	bool write_cmd(const char * text, const int textlen, char* exception, const int eplen,bool acssiif=false);
	/**
	 * 读取响应返回
	 * @param atres {char*} 返回报文信息
	 * @param atreslen {int} 返回报文信息缓存大小
	 * @param exception {char*} 异常描述
	 * @param eplen {int} 异常描述缓存大小
	 * @param acsiif {bool} acsii编码支持，true:返回报文信息为acsii格式,false:返回报文信息为hex格式
	 * @return {bool } 读取是否成功
	 */
	bool read_cmd(char * atres, const int atreslen, char* exception, const int eplen, bool acssiif=false);
	/**
	 * 设置读取数据是循环次数
	 * @param _rc {int} 次数
	 * @return {void } 
	 */
	void setReadCount(int _rc);
	/**
	 * 设置读取数据是循环读取时的等待时间
	 * @param _rc {int} 毫秒
	 * @return {void } 
	 */
	void setReadSleep(int _rc);
private:
	void recvprint( char* data, int len);
private:
	int readSleep;
	int readCount;
};
#endif
