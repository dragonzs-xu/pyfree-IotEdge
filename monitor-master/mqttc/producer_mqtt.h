#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef _PRODUCER_MQTT_H_
#define _PRODUCER_MQTT_H_
/***********************************************************************
  *Copyright 2020-03-06, pyfree
  *
  *File Name       : producer_mqtt.h
  *File Mark       :
  *Summary         : mqtt通信的生产类接口实现
  *
  *
  *Current Version : 1.00
  *Author          : PengYong
  *FinishDate      :
  *
  *Replace Version :
  *Author          :
  *FinishDate      :

 ************************************************************************/
#include "lib_acl.h"
#include "acl_cpp/lib_acl.hpp"

#include "mqttFunc.h"

class PyMQTTIO;

class ProducerMQTT : public acl::thread
{
public:

	ProducerMQTT(PyMQTTIO *mqttcpp_);

	virtual ~ProducerMQTT();
	//
	void* run();
private:
	/**
	 * 向mqtt服务发送点信息
	 * @return {void}
	 */
	void send_pval();
	/**
	 * 向mqtt服务发送回复信息
	 * @return {void}
	 */
	void send_reply();
	/**
	 * 向mqtt服务发送消息
	 * @param buf {const char* } 内容指针
	 * @param len {int } 内容长度
	 * @return {bool} 发送是否成功
	 */
	bool sendMsg(const char* buf, int len);
	//禁止拷贝构造和赋值运算
	ProducerMQTT(const ProducerMQTT&);
	ProducerMQTT& operator=(const ProducerMQTT&) { return *this; };
private:
	bool running;		//线程运行标记
	PyMQTTIO *mqttcpp;	//mqtt通信接口
	MQTTFUNC mqttfunc;	//mqtt业务逻辑处理
	std::string pub_s;	//发布主题
};

#endif 
