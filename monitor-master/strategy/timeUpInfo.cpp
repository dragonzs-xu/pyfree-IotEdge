#include "timeUpInfo.h"

#include <time.h>
#include "business_def.h"
#ifdef WIN32
#define usleep(x) Sleep(x)
#endif

TimeUpPInfo::TimeUpPInfo() 
	: running(true)
	, ptr_bdef(BusinessDef::getInstance())
{
	sendTimeFlag = static_cast<unsigned int>(time(NULL))+100;
	upTimeInterval = ptr_bdef->getTimeUpInterval();
	checkTimeFlag = static_cast<unsigned int>(time(NULL)) + 120;
	checkTimeInterval = ptr_bdef->getTimeCheckInterval();
};

TimeUpPInfo::~TimeUpPInfo()
{
	running = false;
};

void* TimeUpPInfo::run()
{
	while (running)
	{
		if (sendTimeFlag < static_cast<unsigned int>(time(NULL))) 
		{
			sendTimeFlag = static_cast<unsigned int>(time(NULL)) + upTimeInterval;
			ptr_bdef->TimeUpVirtualPInfo();
		}
		if (checkTimeFlag < static_cast<unsigned int>(time(NULL))) 
		{
			checkTimeFlag = static_cast<unsigned int>(time(NULL)) + checkTimeInterval;
			ptr_bdef->TimeCheckPInfo();
		}
		usleep(100);
	}
	return 0;
};
