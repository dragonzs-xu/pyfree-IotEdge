#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#ifndef TIME_UP_INFO_H
#define TIME_UP_INFO_H
/***********************************************************************
  *Copyright 2020-03-06, pyfree
  *
  *File Name       : timeUpInfo.h
  *File Mark       : 
  *Summary         : 
  *虚拟信息点（type=3|4)的数值是否定时推送,有别于变位上送.
  *
  *Current Version : 1.00
  *Author          : pyfree
  *FinishDate      :
  *
  *Replace Version :
  *Author          :
  *FinishDate      :

 ************************************************************************/
#include "lib_acl.h"
#include "acl_cpp/lib_acl.hpp"

class BusinessDef;

class TimeUpPInfo : public acl::thread
{
public:
	TimeUpPInfo();
	~TimeUpPInfo();
	void* run();
private:
	bool running;
	BusinessDef *ptr_bdef;
	unsigned int sendTimeFlag;		//虚拟点上送时间点
	unsigned int upTimeInterval;	//虚拟点上送时间间隔
	unsigned int checkTimeFlag;		//信息点校验时间点
	unsigned int checkTimeInterval; //信息点校验时间间隔
};


#endif
