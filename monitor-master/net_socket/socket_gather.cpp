#include "socket_gather.h"

#include "socket_private_acl.h"
#include "socket_srv_acl.h"
#include "socket_gather_read.h"
#include "socket_gather_write.h"
#include "socket_cache_thread.h"
#include "Log.h"
////////////////////////////////////////////////*SocketGather*///////////////////////////////////////////////////////////////

SocketGather::SocketGather(std::string ip,unsigned int port)
{
	socket_acl = new SocketPrivate_ACL(port,ip);
	if(socket_acl->onConnect()>0)
	{
		socket_srv_acl = new SocketSrv_ACL();
		socket_srv_acl->setPDataPtr(socket_acl);
		socket_srv_acl->start();

		socket_g_read = new SocketGatherRead();
		socket_g_read->setPrivateDataPtr(socket_acl);
		socket_g_read->start();

		socket_g_write = new SocketGatherWrite();
		socket_g_write->setPrivateDataPtr(socket_acl);
		socket_g_write->start();

		socket_cache_thread = new SocketCacheThread();
		socket_cache_thread->start();
	}else{
		socket_srv_acl=NULL;
		socket_g_read=NULL;
		socket_g_write = NULL;
		socket_cache_thread = NULL;
		CLogger::createInstance()->Log(MsgError,
			"listen port(%d) error, [%s %s %d]", port
			, __FILE__, __FUNCTION__, __LINE__);
	}
}

SocketGather::~SocketGather(void)
{
	if(NULL!= socket_g_read)
	{
		delete socket_g_read;
		socket_g_read = NULL;
	}
	if (NULL != socket_g_write) 
	{
		delete socket_g_write;
		socket_g_write = NULL;
	}
	if (NULL != socket_cache_thread) 
	{
		delete socket_cache_thread;
		socket_cache_thread = NULL;
	}
	if (NULL != socket_srv_acl) 
	{
		delete socket_srv_acl;
		socket_srv_acl = NULL;
	}
	if (NULL != socket_acl) 
	{
		delete socket_acl;
		socket_acl = NULL;
	}
}

int SocketGather::Write(const char* buf, int size)
{
	if (NULL != socket_acl) 
	{
		return socket_acl->Write(buf, size);
	}
	else 
	{
		return -1;
	}
}
