#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#ifndef _UDP_IO_CLIENT_H_
#define _UDP_IO_CLIENT_H_
/***********************************************************************
  *Copyright 2020-06-06, pyfree
  *
  *File Name       : udp_io_client.h
  *File Mark       :
  *Summary         : 
  *数据存储通信接口实现,采用udp方式通信,作为发送方,单向推送业务数据到记录软件
  *
  *Current Version : 1.00
  *Author          : PengYong
  *FinishDate      :
  *
  *Replace Version :
  *Author          :
  *FinishDate      :

 ************************************************************************/
#include "lib_acl.h"
#include "acl_cpp/lib_acl.hpp"

#include "datadef.h"
#include "queuedata_single.h"

class UdpIOClient : public acl::thread
{
private:
    /* data */
    bool running;		    //线程运行标记
    std::string local_addr; //
    std::string peer_addr; //
    QueueDataSingle<RecordItem> *queuefor_record;	//记录缓存队列
public:
    UdpIOClient(std::string paddr="127.0.0.1",int pport=60004
        ,std::string laddr="127.0.0.1",int lport=70004);
    ~UdpIOClient();

    void* run();
private:
    int getBuf(unsigned char* buf, int size);
};

#endif
