#include "hashmap.h"

#ifdef WIN32
#include <Winsock2.h>
#else
#include <netinet/in.h>
#include <arpa/inet.h>
#endif

#include "pfunc.h"
#include "Log.h"

KeyObj_FT::KeyObj_FT(std::string  _ipStr, int _id, pyfree::PType _type)
	: m_ipStr(_ipStr)
	, m_id(_id)
	, m_type(_type)
{
	m_ip = pyfree::ipToInt(_ipStr);
};
//
long KeyObj_FT::cmp_Key(const KeyObj_FT &obj1, const KeyObj_FT &obj2)
{
	long diff = static_cast<long>(obj1.m_ip - obj2.m_ip );
	if (diff != 0)
	{
		return diff;
	}
	diff = obj1.m_type - obj2.m_type;
	if (diff != 0)
	{
		return diff;
	}
	diff = obj1.m_id - obj2.m_id;
	if (diff != 0)
	{
		return diff;
	}
	return 0;
};

//////////////////////////////////////////////

KeyObj_TF::KeyObj_TF( long long _devID, int _pID)
	: m_devID(_devID), m_pID(_pID)
{

};
//
long long KeyObj_TF::cmp_Key(const KeyObj_TF &obj1, const KeyObj_TF &obj2)
{
	long long diff = obj1.m_devID - obj2.m_devID;
	if (diff != 0)
	{
		return diff;
	}
	diff = obj1.m_pID - obj2.m_pID;
	if (diff != 0)
	{
		return diff;
	}
	return 0;
};


////////////////////////////////////////
KeyObj_Client::KeyObj_Client(std::string  _ipStr, int _port)
	: m_ipStr(_ipStr)
	, m_port(_port)
{
	m_ip = pyfree::ipToInt(_ipStr);
};
//
long KeyObj_Client::cmp_Key(const KeyObj_Client &obj1, const KeyObj_Client &obj2)
{
	long diff = static_cast<long>( obj1.m_ip - obj2.m_ip );
	if (diff != 0)
	{
		return diff;
	}
	diff = obj1.m_port - obj2.m_port;
	if (diff != 0)
	{
		return diff;
	}
	return 0;
};

