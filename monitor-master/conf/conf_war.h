#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef _CONF_WAR_H_
#define _CONF_WAR_H_
/***********************************************************************
  *Copyright 2020-03-06, pyfree
  *
  *File Name       : conf_war.h
  *File Mark       : 
  *Summary         : 告警信息配置
  *
  *Current Version : 1.00
  *Author          : pyfree
  *FinishDate      :
  *
  *Replace Version :
  *Author          :
  *FinishDate      :

 ************************************************************************/
#include <string>

#include "dtypedef.h"

using namespace pyfree;

//事件信息
struct EventForWaring
{
	//默认构造
	EventForWaring()
		: type(DefAlarmType), desc("DefAlarmType")
		, grade(DefLevel), levelDesc("DefLevel")
		, send_(AlarmForDef), wayDesc("DefWay")
		, execTime("")
		, taskID(0), taskDesc("UnDefTask")
		, devID(0), devDesc("UnDefDev")
		, pID(0), pDesc("UnDefPoit")
		, valDesc(""), Comment("")
	{

	};
	EventForWaring& operator = (const EventForWaring& rhs) //赋值符重载  
	{
		if (this == &rhs)
		{
			return *this;
		}
		type = rhs.type;
		desc = rhs.desc;
		grade = rhs.grade;
		levelDesc = rhs.levelDesc;
		send_ = rhs.send_;
		wayDesc = rhs.wayDesc;
		execTime = rhs.execTime;
		taskID = rhs.taskID;
		taskDesc = rhs.taskDesc;
		devID = rhs.devID;
		devDesc = rhs.devDesc;
		pID = rhs.pID;
		pDesc = rhs.pDesc;
		valDesc = rhs.valDesc;
		Comment = rhs.Comment;
		return *this;
	};
	inline void forEvtDesc()
	{
		switch (type)
		{
		case YXChg:
			desc = "YXChg";
			break;
		case YXShake:
			desc = "YXShake";
			break;
		case YXNoChange:
			desc = "YXNoChange";
			break;
		case YXNoUpdate:
			desc = "YXNoUpdate";
			break;
		case YCUpOverLimit:
			desc = "YCUpOverLimit";
			break;
		case YCDownOverLimit:
			desc = "YCDownOverLimit";
			break;
		case YCBreak:
			desc = "YCBreak";
			break;
		case YCDisturb:
			desc = "YCDisturb";
			break;
		case YCNoChange:
			desc = "YCNoChange";
			break;
		case YCNoUpdate:
			desc = "YCNoUpdate";
			break;
		case YKOpFail:
			desc = "YKOpFail";
			break;
		case YTOpFail:
			desc = "YTOpFail";
			break;
		case UserDefCondition:
			desc = "UserDefCondition";
			break;
		case UserDefRestart:
			desc = "UserDefRestart";
			break;
		case UserDefRout:
			desc = "UserDefRout";
			break;
		case UserDefTime:
			desc = "UserDefTime";
			break;
		case DefAlarmType:
			desc = "DefAlarmType";
			break;
		default:
			desc = "UserNewDefType";
			break;
		}
	};
	inline void forLevelDesc()
	{
		switch (grade)
		{
		case normalLevel:
			levelDesc = "normal";
			break;
		case majorLevel:
			levelDesc = "major";
			break;
		case seriousLevel:
			levelDesc = "serious";
			break;
		case DefLevel:
			levelDesc = "DefLevel";
			break;
		default:
			levelDesc = "UserNewDefLevel";
			break;
		}
	};
	inline void forWayDesc()
	{
		switch (send_)
		{
		case AlarmForLog:
			wayDesc = "Log";
			break;
		case AlarmForSMS:
			wayDesc = "SMS";
			break;
		case AlarmForEMail:
			wayDesc = "EMail";
			break;
		case AlarmForDef:
			wayDesc = "DefWay";
			break;
		default:
			wayDesc = "UserNewDefWay";
			break;
		}
	};
	EventType	type;		//事件类型
	std::string desc;		//事件描述
	EventLevel	grade;		//事件等级
	std::string levelDesc;  //等级描述
	EventWay	send_;		//通知方式
	std::string wayDesc;	//通知方式描述
	std::string execTime;	//时间
	unsigned long taskID;	//任务编号
	std::string taskDesc;	//任务描述
	unsigned long devID;	//设备编号
	std::string devDesc;	//设备描述
	unsigned long pID;		//点编号
	std::string pDesc;		//点描述
	std::string valDesc;	//值状况描述
	std::string Comment;	//告警内容
};

#endif
