#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#ifndef VERIFICATION_FOR_CONTROL_CACHE_H
#define VERIFICATION_FOR_CONTROL_CACHE_H
/*
下控指令的缓存信息，用于其后续结果校验
*/
#include <list>
#ifdef __linux__
#include <string>
#endif

#include "Mutex.h"
#include "dtypedef.h"
#include "conf_war.h"
#include "queuedata_single.h"

using namespace pyfree;

class BusinessDef;

struct VerificationCache
{
	VerificationCache()
		: taskID(0)
		, taskDesc("")
		, devID(0)
		, devDesc("")
		, pID(0)
		, pDesc("")
		, pType(0)
		, val(0.0)
		, val_(0.0)
		, execTime("")
		, limitTimeForCheck(0)
		, eway_(AlarmForDef)
	{};
	VerificationCache& operator = (const VerificationCache& rhs) //赋值符重载  
	{
		if (this == &rhs)
		{
			return *this;
		}
		taskID		= rhs.taskID;
		taskDesc	= rhs.taskDesc;
		devID		= rhs.devID;
		devDesc		= rhs.devDesc;
		pID			= rhs.pID;
		pDesc		= rhs.pDesc;
		pType		= rhs.pType;
		val			= rhs.val;
		val_		= rhs.val_;
		execTime	= rhs.execTime;
		limitTimeForCheck = rhs.limitTimeForCheck;
		eway_		= rhs.eway_;
		return *this;
	};
	unsigned long	taskID;
	std::string		taskDesc;
	unsigned long	devID;
	std::string		devDesc;
	unsigned long	pID;
	std::string     pDesc;
	unsigned int	pType;
	float			val;
	float			val_;
	std::string		execTime;
	unsigned int	limitTimeForCheck;
	EventWay		eway_;
};

class VerifyForControlCache
{
public:
	static VerifyForControlCache* getInstance();
	static void Destroy();
	~VerifyForControlCache();

	//////////////////////////////////////////////////////////////////
	void addVerifyData(VerificationCache it);
	void checkVerifyData();
private:
	VerifyForControlCache();
	VerifyForControlCache& operator=(const VerifyForControlCache&) { return *this; };
	void init();
	bool comFloatVal(float valA, float valB);
private:
	static VerifyForControlCache* instance;
	static float	VALLIMIT;
	BusinessDef	*ptr_CacheDataObj;
	QueueDataSingle<EventForWaring> *queueforwar;//告警事件缓存队列
	//the control return  is be verified
	std::list<VerificationCache> VerifyData;
	PYMutex m_MutexVerify;
};
#endif

